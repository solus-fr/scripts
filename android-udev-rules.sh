#!/bin/bash

cd /tmp

# Clone this repository
git clone https://github.com/M0Rf30/android-udev-rules.git
cd android-udev-rules

# Copy rules file
sudo cp -v 51-android.rules /etc/udev/rules.d/51-android.rules

# Change file permissions
sudo chmod a+r /etc/udev/rules.d/51-android.rules

# Restart UDEV
sudo service udev restart

