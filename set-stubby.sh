#!/bin/bash
if [ "$UID" -ne "0" ] #User check
then
   echo -e "Use this script as root."
   exit
else

eopkg it -y stubby

chattr -i /etc/resolv.conf #Allow the modification of the file

sed -i 's|nameserver|#nameserver|' /etc/resolv.conf #Disable previous DNS servers

echo -e "nameserver 127.0.0.1" >> /etc/resolv.conf #Set localhost

echo "Edit the conf file to choose wich DNS server to choose."

gedit /usr/share/defaults/stubby/stubby.yml

systemctl start stubby 

systemctl enable stubby 

echo
echo
echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Now check on https://dnsleaktest.com/"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo

fi
