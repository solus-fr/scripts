#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O stremio.AppImage https://dl.strem.io/linux/v4.0.17/Stremio+4.0.17.appimage

chmod +x stremio.AppImage

./stremio.AppImage
