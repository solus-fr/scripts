#!/bin/bash

cd /tmp

sh -c "$(curl https://raw.githubusercontent.com/cjbassi/gotop/master/scripts/download.sh)"

sudo cp gotop /usr/bin

sudo chmod +x /usr/bin/gotop

echo
echo
echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "now, just type gotop"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo
