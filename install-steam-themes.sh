#!/bin/bash
#

sudo eopkg -y it git font-roboto-ttf

mkdir -p ~/.steam/steam/skins/

cd ~/.steam/steam/skins/

git clone https://github.com/Outsetini/Air-for-Steam.git

git clone https://github.com/DirtDiglett/Pressure-for-Steam.git

git clone https://github.com/DirtDiglett/Pressure2.git

git clone https://github.com/tkashkin/eOSSteamSkin.git

git clone https://github.com/Edgarware/Threshold-Skin.git

git clone https://github.com/somini/Pixelvision2.git

git clone https://github.com/badanka/Compact.git

git clone https://github.com/minischetti/metro-for-steam.git

git clone https://github.com/raylee930/flat-steam.git

echo
echo
echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Open your Steam Settings Window. Navigate to Interface on the left,"
echo "and locate the option "Select the skin you wish Steam to use" and choose the skin you want."
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo
