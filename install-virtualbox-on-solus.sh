#!/bin/bash
#

# Remove prior installation (if exists)

sudo /opt/VirtualBox/uninstall.sh

# Update your system

sudo eopkg upgrade -y

sudo eopkg install -y virtualbox-current virtualbox-guest-current

# Extra configuration

sudo gpasswd -a $USER vboxusers

sudo gpasswd -a $USER vboxsf

