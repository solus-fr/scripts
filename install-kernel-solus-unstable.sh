#!/bin/bash 

#sudo eopkg remove-repo Solus
sudo eopkg disable-repo Solus
sudo eopkg add-repo SolusUnstable https://packages.solus-project.com/unstable/eopkg-index.xml.xz
sudo eopkg enable-repo SolusUnstable

sudo eopkg upgrade kernel

#sudo eopkg remove-repo SolusUnstable
sudo eopkg disable-repo SolusUnstable
#sudo eopkg add-repo Solus https://packages.solus-project.com/shannon/eopkg-index.xml.xz
sudo eopkg enable-repo Solus
sudo eopkg upgrade
