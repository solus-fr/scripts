#!/bin/bash
#

## check version here: https://www.torproject.org/projects/torbrowser.html.en

## removing previous version
rm -rf $HOME/tor-browser_fr

cd /tmp

wget -O tor-browser-fr.tar.xz https://dist.torproject.org/torbrowser/8.0/tor-browser-linux64-8.0_fr.tar.xz

tar xvf tor-browser-fr.tar.xz 

mv tor-browser_fr  $HOME/

echo
echo
echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "check your HOME folder for a directory called tor-browser_fr"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo
