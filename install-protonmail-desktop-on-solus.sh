#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O protonmail-desktop.AppImage https://github.com/protonmail-desktop/application/releases/download/v0.5.8/protonmail-desktop-0.5.8-x86_64.AppImage

chmod +x protonmail-desktop.AppImage

./protonmail-desktop.AppImage
