#!/bin/bash
#

cd /tmp

git clone https://github.com/vinceliuice/Sierra-gtk-theme.git

cd Sierra-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/matcha.git

cd matcha && ./Install

cd /tmp

git clone https://github.com/vinceliuice/Qogir-theme.git

cd Qogir-theme && ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/vimix-gtk-themes.git

cd vimix-gtk-themes && ./Vimix-installer

cd /tmp

git clone https://github.com/vinceliuice/Canta-theme.git

cd Canta-theme && ./install.sh

cd /tmp

git clone https://github.com/nana-4/materia-theme.git

cd materia-theme && sudo ./install.sh
