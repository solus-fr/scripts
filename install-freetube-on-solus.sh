#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

# just in case
rm freetube.AppImage

wget -O freetube.AppImage https://github.com/FreeTubeApp/FreeTube/releases/download/v0.4.1-beta/FreeTube-0.4.1-x86_64.AppImage

chmod +x freetube.AppImage

./freetube.AppImage
