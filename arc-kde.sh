#!/bin/bash


# Arc KDE theme

sudo eopkg it -y arc-kde

# paprirus Icon theme

sudo eopkg it -y papirus-icon-theme

#harcode-tray

sudo eopkg it -y hardcode-tray imagemagick

sudo -E hardcode-tray --conversion-tool ImageMagick --size 22 --theme Papirus
