# My scripts for Solus / Mes scripts pour Solus

# Use / Utilisation

`git git@gitlab.com:devil505/scripts.git && cd scripts`

# Scripts

## Bitwarden

_To install password manager client, Bitwarden / Pour installer le client de gestion de mots de passe, Bitwarden_

`./install-bitwarden-on-solus.sh`   

## Molotov 

_To install Molotov to watch TV channels / Pour installer Molotov pour visionner des chaînes de Télé_ 

`./install-molotov-on-solus.sh`    

## Stremio 

_To install Stremio for watching movies and series by streaming / Pour installer Stremio pour visionenr films et séries en streaming_     

`./install-stremio-on-solus.sh`

## Update-hosts

_To modify your /etc/hosts file by adding [Energized Linux](https://github.com/EnergizedProtection/Energized_Linux) basic blacklist to prevent ads, trackers...(different levels available) / Ceci va modifier votre fichier /etc/hosts en y ajoutant la liste noire basique  Energized Linux](https://github.com/EnergizedProtection/Energized_Linux) pour vous protéger des pubs, traçeurs (différents niveaux dosponibles..._

`./set-energized.sh`

Same script but with ultimate + extensions (porn and social networks) / Même script mais avec la liste ultimate + extensions (porno et réseaux sociaux)

`./set-energized-extreme.sh`

## Cozy

_To install Cozy Cloud client / Pour installer le client Cozy Cloud_

`./install-cozy-on-solus.sh`

## Oh My ZSH !

_To install Oh My ZSH! sur Solus / Pour installer Oh My ZSH! sur Solus_

`./install-oh-my-zsh-on-solus.sh`

## Tor Browser FR

_To install Tor Browser (French version) on Solus / Pour installer le navigateur Tor (version française) sur Solus_

`./install-torbrowser-fr.sh`

## French Data Network DNS

_To force the use of the DNS of FDN (french association respecting privacy) / Pour forcer l'utilsiation des DNS de la FDN (association française respectueuse de la vie privée)_

`sudo ./set-FDN-dns.sh`

## Gotop

_To install the top monitor written in Go language, gotop / Pour installer le moniteur top écrit en langage Go, gotop_

`./install-gotop.sh`

## Powerline9K

_To install Powerline9k theme on your zsh shell / Pour installer le thème Powerline9k à votre shell zsh_

`./install-powerlevel9k-on-zsh.sh`

## VirtualBox

_To install VirtualBox on Solus / Pour installer VirtualBox sur Solus_

`./install-virtualbox-on-solus.sh`

## OpenNIC DNS

_This script will force the use nearest DNS from OpenNIC / Ce script force l'utilisation des DNS les plus proches d'OpenNIC_

`sudo ./set-opennic-dns.sh`

## Joplin

_To install the Notes taking software, joplin (supporting Nextcloud, Dropbox...) / Pour installer le logiciel de prises de notes, Joplin (supportant Nextcloud, Dropbox...)_

`./install-joplin-on-solus.sh`

## Protonmail-Desktop

_To install the web interface for ProtonMail, ProtonMail Desktop / Pour installer l'interface web pour ProtonMail, ProtonMail Desktop_

`./install-protonmail-desktop-on-solus.sh`

## Whalebird

_To install Whalebird (Mastodon client) / Pour installer le client Mastodon Wahelbird_

`./install-whalebird.sh`

## Kernel Unstable

_To install the kernel from Solus unstable repo (not recommended) / Pour installer le kernel du dépôt instable de Solus (non recommandé)_

`./install-kernel-solus-unstable.sh`

## ProtonVPN-CLI

_To install protonVPN CLI software / Pour installater le logiciel porotonVPN CLI_

`./install-protonvpn-cli.sh`

## Gnome Themes / Thèmes GNOME

_To install more GNOME Themes / Pour installer des thèmes GNOME supplémentaires_

`./install-gnome-themes.sh`

## Steam Themes / Thèmes Steam

_To install themes for steam / Pour installer des thèmes pour steam_

`./install-steam-themes.sh`

## Freetube

_To install Freetube, Youtube app respecting privacy / Pour installer Freetube, application youtube respectant l'anonymat_

`./install-freetube-on-solus.sh`

## Wallhallapapers

_To install wallpapers from [zarchbox](https://zarchbox.fr/storage/wallpapers/)/ Pour installer les fons d'écrans de [zarchbox](https://zarchbox.fr/storage/wallpapers/)_

`./wallhallapapers.sh`

## Stubby

_To install and set up stubby for DNS over TLS / Pour installer et configurer Stubby pour le DNS over TLS_

`./set-stubby.sh`

