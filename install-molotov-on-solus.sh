#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O molotov.AppImage http://desktop-auto-upgrade.molotov.tv/linux/2.3.0/molotov.AppImage

chmod +x molotov.AppImage

./molotov.AppImage
