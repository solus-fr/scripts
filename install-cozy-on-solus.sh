#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O cozy64.AppImage https://github.com/cozy-labs/cozy-desktop/releases/download/v3.10.3-beta.1/CozyDrive-3.10.3-beta.1-x86_64.AppImage

chmod +x cozy64.AppImage

./cozy64.AppImage
