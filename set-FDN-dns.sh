#!/bin/bash
if [ "$UID" -ne "0" ] #User check
then
   echo -e "Use this script as root."
   exit
else

# https://www.fdn.fr/actions/dns/

chattr -i /etc/resolv.conf #Allow the modification of the file

sed -i 's|nameserver|#nameserver|' /etc/resolv.conf #Disable previous DNS servers

echo -e "nameserver 80.67.169.12
nameserver 80.67.169.40" >> /etc/resolv.conf #Set the FDN DNS servers

chattr +i /etc/resolv.conf #Disallow the modification of the file

fi
