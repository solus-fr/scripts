#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O email-securely-app.AppImage https://github.com/vladimiry/email-securely-app/releases/download/v2.0.0-beta.5/email-securely-app-2.0.0-beta.5-linux-x86_64.AppImage

chmod +x email-securely-app.AppImage

./email-securely-app.AppImage
