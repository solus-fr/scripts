#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O Joplin.AppImage https://github.com/laurent22/joplin/releases/download/v1.0.81/Joplin-1.0.81-x86_64.AppImage

chmod +x Joplin.AppImage

./Joplin.AppImage
