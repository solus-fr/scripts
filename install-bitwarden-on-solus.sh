#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O bitwarden.AppImage https://github.com/bitwarden/desktop/releases/download/v1.10.0/Bitwarden-1.10.0-x86_64.AppImage

chmod +x bitwarden.AppImage

./bitwarden.AppImage
